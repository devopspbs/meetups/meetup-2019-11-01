<!-- .slide: data-background-image="images/meetup.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## O Que Temos Para Hoje

* Introdução a bot no Telegram - Wilker Paz
* Arquitetura de um aplicativo mobile - Thiago Almeida

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."

[//]: # (https://garoa.net.br/wiki/C%C3%B3digo_de_Conduta_Completo)

----  ----

## Site e Mídias Sociais

* **Site oficial:** [devopspbs.org](https://devopspbs.org)

* **Mídias sociais:**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)
  - [instagram/devopspbs](https://www.instagram.com/devopspbs/)

* **Grupos de discussão:**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Apoio:

<a href="https://www.facebook.com/PEBASFOOD/"><img height="200" src="images/foodpebas.jpg" alt="FoodPebas"></a>

----  ----

## Dica: Linkedin Mobile

![](images/in.png)
