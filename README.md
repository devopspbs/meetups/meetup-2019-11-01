# Meetup Outubro 2019

## Palestras

[Abertura](https://devopspbs.gitlab.io/meetups/meetup-2019-11-01/)

Introdução a bot no Telegram -  Wilker Paz, Analista Financeiro na Vale S/A.

Arquitetura de um aplicativo mobile - Thiago Almeida, Analista de Sistemas na CMP

> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
